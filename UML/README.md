## Install necessary packages:
```
pip install -U pylint
```

Download and install Graphviz from here http://www.graphviz.org/download/
If you are using Windows, please add the bin folder of Graphviz to your PATH

**Careful!** pylint requires pyqt5<5.13, therefore it is necessary to downgrade pyqt5 and pyqtwebengine to 5.12.0:
```
pip uninstall pyqt5 pyqtwebengine
pip install -I pyqtwebengine==5.12.0 pyqt5==5.12.0
```

## Generating UML Diagrams
To generate UML diagrams, run the following commands in the inside the "code" directory

```
pyreverse CoffeeBarSimulation -A -p PythonProject -o svg
mv *.svg ../UML
```
