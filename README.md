# A coffee bar simulation for five years
We will model a coffee bar that sells drinks and food for five years. They have customers which are either one time or returning.  
Some are returning are hipsters, some one timers are people from tripadvisor .

## PART1: DETERMINE PROBABILITIES

For food, { cookie, muffin, pie, sandwich} is on the sell list. For drinks, {coffee, frappucino, milkshake, soda, tea, water} is on the sell list.
Using [pandas.unique()](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.unique.html) method, we find that there are 247988 unique customers.  
To estimate probability, hat a customer buys a certain food or drink at any given time during a day,  
we divide the number of each food sold per given time by the total number of food sold per given time.

## PART2: CREATE CUSTOMERS
To create customers, we define 5 classes: {Customer, ReturningCustomer, OnetimeCustomer, Purchase Item}.
Note that ReturningCustomer and OnetimeCustomer are subclasses that belong to Customer.
We denote returning customers separately into hipsters and regulars as they have different initial budget, and track their purchase by writing  
a method purchase_history().  
 For onetime customers, we break them into to note whether they are from  tripadvisor or they are regulars.  
  And assign tip from uniformly distribution from 1 to 10 dollars using [randint(1,10)](https://www.geeksforgeeks.org/python-randint-function/)

## PART3 : SIMULATIONS
For simulations, we define another class "Simulation", which makes it easier to reflect when there is any change  
 in the parameters such as proportion of returning customers and one time customers.

```python
class Simulation(object):
    def __init__(self, p_type, p_purchase, prices, budgets, n_returning_customers, daily_time_slots):
        self.pMainType, self.pHipster, self.pTripAdvisor = p_type[0], p_type[1], p_type[2]
        self.pDrink, self.pricesDrink = p_purchase[0], prices[0]
        self.pFood, self.pricesFood = p_purchase[1], prices[1]
        self.returningBudget, self.returningHipsterBudget, self.oneTimeBudget = budgets[0], budgets[1], budgets[2]
        self.revenue, self.tips = [], []
        self.nDailyTimeSlots, self.dailyTimeSlots = len(daily_time_slots), daily_time_slots
```
 Generate 1000 returning customers with specified proportion of 20% (This is specfied in the main.py as "p_type" object.).
 We track the returning customers' purchase history and tips from calling Customer class that we defined in part 2.

For every data generation in the simulation, [numpy.random.choice(p= )](https://numpy.org/doc/stable/reference/random/generated/numpy.random.choice.html) is used.
For example, here we are given proportion of Hipster is one third (p=self.pHipster).

```python
       for i in range(1, self.nReturningCustomers + 1):
            cid = "CID_r" + str(i)
            if not numpy.random.choice([True, False], p=self.pHipster):
                self.returningCustomers.append(ReturningCustomer(cid, False, self.returningBudget))
            else:
                self.returningCustomers.append(ReturningCustomer(cid, True, self.returningHipsterBudget))
```

One key challenge is to make sure that returning customers with no money left will not make pseudo purchase.
In other words, we need to exclude returning customers that ran out of their budget. We implement this task with the following loop inside start(start_date, num_days) method.

```python
           for i in range(0, self.nDailyTimeSlots):
                choice = numpy.random.choice([0, 1], p=self.pMainType)
                if choice == 0 and returning_customers_copy:
                    random_returning_customer = numpy.random.choice(returning_customers_copy)
                    while random_returning_customer.budget < self.most_expensive_purchase() and returning_customers_copy:
                        returning_customers_copy.remove(random_returning_customer)
                        if returning_customers_copy:  # if customerListR_tmp is not empty
                            random_returning_customer = numpy.random.choice(returning_customers_copy)
                    # if customer is a returning customer, then append to list of customers a random regular customer
                    if returning_customers_copy:
                        daily_customer_list.append(random_returning_customer)
                    else:
                        special = True
```
 Note that we update "returning_customers_copy" everytime there is a purchase from a returning customer. When their budge hits lower than the sum of the most expensive drink and food,
 we exclude them from the "returning_customers_copy".

## PART4 : ADDITIONAL QUESTIONS
With the pre-written classes including "Simulation" class, it is easy to do the whole process again with the change of parameters.
To see buying histories of returning customers, we use [pivot_table( , aggfunc ='size')](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.pivot\_table.html) from Pandas.
With the specification "aggfunc='size'", duplicates are counted and it generates the returning customers with the frequency of their visits over the five years.

```
  # counting the duplicates
dups = df.pivot_table(index=['CUSTOMER'], aggfunc='size')

returncustomer_dups = []
mostlylikelytime = []
DUPS = []
for i in range(0,len(dups)):
    if dups[i]>1:
         returncustomer_dups +=[CUSTOMER[i], dups[i], TIME[i]]
         DUPS +=  [dups[i]]
         print(CUSTOMER[i], dups[i], TIME[i])
```



## Authors
* ** Martin Rohar, Lewis Shin Heuk Kang**



