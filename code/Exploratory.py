import pandas as pd
import matplotlib.pyplot as plt

#########
# Options
#########
pd.set_option('display.max_columns', 20)
pd.set_option('display.max_rows', 100)
pd.set_option('display.width', 1000)

####################
# Load the data file
####################
df = pd.read_csv("../data/Coffeebar_2016-2020.csv", delimiter=";")
print(df.head())
df["TIME"] = pd.to_datetime(df.TIME)
df['YEAR'] = df['TIME'].dt.strftime('%Y')

######################
# Types of drinks sold
######################
print("The shop is selling the following drinks: %s" % ', '.join(df.DRINKS.unique()))

############################
# Number of unique customers
############################
print("There are %s unique customers" % len(df.CUSTOMER.unique()))

########################################################################
# Plot1 - bar plot of the total amount of sold foods over the five years
########################################################################
food = df["FOOD"].groupby([df["YEAR"], df["FOOD"]]).count().unstack(level=1)
print(food)
food.plot(kind="bar")
plt.title("Total Sales of Food by category (2016-2020)")
plt.xlabel("Year")
plt.ylabel("Amount of Food sold")

# labels
i = 1 / (len(food) - 1)  # 1 divided by the number of rows to get the initial offset
for column in food:
    for index, data in enumerate(food[column]):
        plt.text(x=index - i, y=data + 30, s=f"{data}", size=9)
    i = i - (1 / (len(food.columns))) / 2  # divide the initial i by 2 to adjust the placing od data labels
plt.tight_layout()
plt.show()

####################################################################
# Plot2 - Bar plot of the total amount of drinks over the five years
####################################################################
drinks = df["DRINKS"].groupby([df["YEAR"], df["DRINKS"]]).count().unstack(level=1)
print(drinks)
drinks.plot(kind="bar")
plt.title("Total Sales of Drinks by category (2016-2020)")
plt.xlabel("Year")
plt.ylabel("Amount of Drinks sold")

i = 1 / (len(drinks) - 1)  # 1 divided by the number of rows to get the initial offset
for column in drinks:
    for index, data in enumerate(drinks[column]):
        plt.text(x=index - i, y=data + 30, s=f"{data}", size=6)
    # (1 divided by the initial total number of data columns) and then divided by 2 to adjust the offset for data labels
    i = i - (1 / (len(drinks.columns))) / 2
plt.tight_layout()
plt.show()

##########################
# Plot 3 - Revenue vs Time
##########################

# MISSING?

########################
# Generate probabilities
########################
df['TIME1'] = df['TIME'].dt.strftime('%H:%M')  # New column for time (HH:MM) only
# Food count by hour
food_probabilities = (df["FOOD"].groupby([df["TIME1"], df["FOOD"].fillna("tmp")])
                      .count()
                      .unstack(level=1)
                      .fillna(0)
                      .drop(["tmp"], axis=1))
# Drinks count by hour
drinks_probabilities = (df["DRINKS"].groupby([df["TIME1"], df["DRINKS"]])
                        .count()
                        .unstack(level=1)
                        .fillna(0))
# Calculate probabilities
tmp_list = [drinks_probabilities, food_probabilities]
for item in tmp_list:
    item["total"] = item.sum(axis=1)
    for column in item:
        item[column] /= item["total"]

# Generate aggregate table of probabilities
total_probabilities = pd.concat([tmp_list[0], tmp_list[1]], axis=1, join='inner')
total_probabilities = total_probabilities.drop(["total"], axis=1)
total_probabilities = total_probabilities.fillna(0)
print(total_probabilities)

# Save probabilities to csv file
food_probabilities.to_csv('../data/food_probabilities.csv', index=True)
drinks_probabilities.to_csv('../data/drinks_probabilities.csv', index=True)
total_probabilities.to_csv('../data/total_probabilities.csv', index=True)
