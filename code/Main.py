import pandas as pd
from CoffeeBarSimulation.Simulation import Simulation
from CoffeeBarSimulation.Customer import Item
import matplotlib.pyplot as plt

###############
# PROBABILITIES
###############
Total = pd.read_csv("../data/total_probabilities.csv")
T = len(Total.TIME1)

# Define probabilities of onetime/returning customers
Type = [0.2, 0.8]  # [Returning Customer, Onetime Customer]
Hipster = [1 / 3, 2 / 3]  # [Hipsters, Normal]
TripAdvisor = [0.1, 0.9]  # [Tripadvisor, Normal]

# Define probabilities of buying drinks/food
DrinkDistribution, FoodDistribution = [], []
for t in range(0, T):
    DrinkDistribution.append([Total.coffee[t], Total.frappucino[t], Total.milkshake[t], Total.soda[t], Total.tea[t],
                              Total.water[t]])
    FoodDistribution.append([Total.cookie[t], Total.muffin[t], Total.pie[t], Total.sandwich[t]])
###############

########
# PRICES
########
drinks = [Item("coffee", 3), Item("frappucino", 4), Item("milkshake", 5), Item("soda", 3), Item("tea", 3),
          Item("water", 2)]
food = [Item("cookie", 2), Item("muffin", 3), Item("pie", 3), Item("sandwich", 2)]
########

############################
# NUMBER OF DAYS TO SIMULATE
############################
start_date = "1/1/2016"
num_days = 365 * 5
dailyTimeSlots = Total.TIME1
############################

#####################
# RETURNING CUSTOMERS
#####################
nReturning = 1000  # number of returning customers
returningBudget = 250  # budget for normal returning customers
hipsterBudget = 500  # budget for hipsters returning customers
onetimeBudget = 100  # budget for onetime customers, (UNUSED)
#####################

simulation1 = Simulation([Type, Hipster, TripAdvisor], [DrinkDistribution, FoodDistribution], [drinks, food],
                         [returningBudget, hipsterBudget, onetimeBudget], nReturning, dailyTimeSlots)

simulation1.start(start_date, num_days)
simulation1.plot("revenue", True)

###########################################################################
# 1) Show some buying histories of returning customers for your simulations
###########################################################################
simulation1.display_returning_customers_purchase_history()

#############################################################
# 2) In the original dataset, how many returning customers?
#############################################################
df = pd.read_csv("../data/Coffeebar_2016-2020.csv", delimiter=";")
TIME = pd.to_datetime(df.TIME)
CUSTOMER = df["CUSTOMER"]

# counting the duplicates
dups = df.pivot_table(index=['CUSTOMER'], aggfunc='size')
returncustomer_dups = []
mostlylikelytime = []
DUPS = []
for i in range(0, len(dups)):
    if dups[i] > 1:
        returncustomer_dups += [CUSTOMER[i], dups[i], TIME[i]]
        DUPS += [dups[i]]
        mostlylikelytime += [TIME[i]]
        print("ID:" + str(CUSTOMER[i]) + " frequency:" + str(dups[i]) + " time:" + str(TIME[i]))
        # print("ID: %s"CUSTOMER[i], "frequency:" %dups[i], "time:" % TIME[i])

print("There are %s returning customers" % len(DUPS))


##############################################################################################
# 3) What would happen if we lower the returning customers to 50 and simulate the same period?
##############################################################################################
nReturning_4 = 50

simulation2 = Simulation([Type, Hipster, TripAdvisor], [DrinkDistribution, FoodDistribution], [drinks, food],
                         [returningBudget, hipsterBudget, onetimeBudget], nReturning_4, dailyTimeSlots)
simulation2.start(start_date, num_days)

simulation2.display_returning_customers_purchase_history()
simulation2.plot("revenue", True)

##################################################################
# 4) The prices change from the beginning of 2018 and go up by 20%
##################################################################
simulation3 = Simulation([Type, Hipster, TripAdvisor], [DrinkDistribution, FoodDistribution], [drinks, food],
                         [returningBudget, hipsterBudget, onetimeBudget], nReturning, dailyTimeSlots)
simulation3.start(start_date, 731)  # Run the simulation from 1/1/2016 till 12/31/2017

drinks_20up = [Item("coffee", 3 * 1.2), Item("frappucino", 4 * 1.2), Item("milkshake", 5 * 1.2), Item("soda", 3 * 1.2),
               Item("tea", 3 * 1.2), Item("water", 2 * 1.2)]  # increase prices by 20%
simulation4 = Simulation([Type, Hipster, TripAdvisor], [DrinkDistribution, FoodDistribution], [drinks_20up, food],
                         [returningBudget, hipsterBudget, onetimeBudget], nReturning, dailyTimeSlots)
start_date_2 = "1/1/2018"
simulation4.returningCustomers = simulation3.returningCustomers  # copy returning customers from simulation3 (2016-2017)
simulation4.start(start_date_2, 730)  # run the simulation from 1/1/2018 till 12/31/2020

simulation4.display_returning_customers_purchase_history()

simulation3.revenue = simulation3.revenue.append(simulation4.revenue)
#simulation3.plot("revenue", True)

#######################################
# 5) The budget of hipsters drops to 40
#######################################
hipsterBudget = 40  # budget for hipsters returning customers

simulation5 = Simulation([Type, Hipster, TripAdvisor], [DrinkDistribution, FoodDistribution], [drinks, food],
                         [returningBudget, hipsterBudget, onetimeBudget], nReturning, dailyTimeSlots)
simulation5.start(start_date, num_days)

simulation5.display_returning_customers_purchase_history()
simulation5.plot("revenue", True)

