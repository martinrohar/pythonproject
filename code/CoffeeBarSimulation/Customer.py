from random import randint


class Item(object):
    """
    A class used to save each Customer ID and the price of each item (purchase)

     Attributes
    ----------
    name : str
        Customer ID
    price : int
        price of an item
    """
    def __init__(self, name: str, price: int):
        self.name = name
        self.price = price


class Customer(object):
    """
    A class used to represent a Customer

     Attributes
    ----------
    purchases : list
        a formatted string to print out what the animal says
    customerID : str
        Customer ID
    budget : int
        budget of each customer
    tip : int
        the amount of tip from each customer

    Methods
    -------
    buy(item, tip)
        append the list of purchase and tips, and compute the remaining budget.
    """
    def __init__(self, customer_id: str, budget: int):
        self.purchases = []
        self.customerID = customer_id
        self.budget = budget
        self.tip = 0

    def buy(self, item: Item, tip: int):
        self.purchases.append(item)
        self.budget = self.budget - item.total - tip


class ReturningCustomer(Customer):
    """
    A subclass used to track returning customers's ID and purchase history

     Attributes
    ----------
    purchases : list
        a formatted string to print out what the animal says
    customer_id : str
        Customer ID
    hipster : str
        whether a returning customer is hipster
    budget : int
        the remaining budget of each returning customer

    Methods
    -------
    purchase_history()
        show every purchase of each returning customer until their budget runs out.
        If their budget run out, print "Purchase history is empty"
    """
    def __init__(self, customer_id: str, hipster: bool, budget: int):
        self.hipster = hipster
        Customer.__init__(self, customer_id, budget)

    def purchase_history(self):
        if self.purchases:
            for i in range(0, len(self.purchases)):
                self.purchases[i].display()
        else:
            print("Purchase history is empty")


class OnetimeCustomer(Customer):
    """
    A subclass used to track returning customers's ID and purchase history

     Attributes
    ----------
    purchases : list
        a formatted string to print out what the animal says
    customer_id : str
        Customer ID
    tripadvisor : str
        whether onetime customer is from tripadvisor
    budget : int
        the remaining budget of each returning customer

    """
    def __init__(self, customer_id: str, tripadvisor: bool):
        Customer.__init__(self, customer_id, 100)  # OnetimeCustomer budget hardcoded to 100
        if tripadvisor:
            self.tip = randint(1, 10)
        else:
            self.tip = 0


class Purchase(object):
    """
    A class used to represent purchase

     Attributes
    ----------
    drink_item : str
        item if it is drink
    food_item : str
        item if it is food
    time : str
        the time when an item is bought

    Methods
    -------
    display()
        show the purchase of a customer.
    """
    def __init__(self, drink_item: Item, food_item: Item, time: str):
        self.drinkItem = drink_item
        self.foodItem = food_item
        self.total = drink_item.price + food_item.price
        self.time = time

    def display(self):
        if self.foodItem.name != "NA":
            print("%s: %s for %s AND %s for %s" % (
                self.time, self.drinkItem.name, self.drinkItem.price, self.foodItem.name, self.foodItem.price))
        else:
            print("%s: %s for %s" % (
                self.time, self.drinkItem.name, self.drinkItem.price))
