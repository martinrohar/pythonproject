import numpy
import pandas as pd
import matplotlib.dates as mdates
import os.path as path
from matplotlib import pyplot as plt
from pandas import DataFrame
from .Customer import OnetimeCustomer, ReturningCustomer, Item, Purchase


class Simulation(object):
    """
    A class used for simulation

     Attributes
    ----------
    p_type : list
        specification for probability of onetime customers, tripadvisor onetime customers and returning customers
    p_purchase: list
        specification for probability of the food and drink purchase distribution
    prices: list
        specification for the prices of food and drink
    budgets: list
        specification for budget in each type of customer
    n_returning_customers: int
        specification for the number of returning customers
    daily_time_slots: list
        the list of slots in each day
    nDailyTimeSlots: int
        number of daily time slots for purchase

    Methods
    -------
    set_date(start_date: str, number_of_days: int)
        determine the start date and end date of simulation
    get_date(time_slot_number: int, day_number: int)
        brings the location of time slot and day
    most_expensive_purchase()
        compute the most expensive combination of a food and a drink
    start(start_date: str, num_days: int)
        begin simulation that tracks revenue and tips based on the parameter specification given above
    plot(var, diff)
        plot revenue for the given period where var is "revenue" or "tips", and diff specifies whether to show revenue
        as a difference against the original.
    display_returning_customers_purchase_history()
        print each returning customer's purchase record
    get_original_revenue(n_days)
        brings original revenue for the comparison with the simulated revenue
    """
    def __init__(self, p_type: list, p_purchase: list, prices: list, budgets: list, n_returning_customers: int,
                 daily_time_slots: list):
        self.pMainType, self.pHipster, self.pTripAdvisor = p_type[0], p_type[1], p_type[2]
        self.pDrink, self.pricesDrink = p_purchase[0], prices[0]
        self.pFood, self.pricesFood = p_purchase[1], prices[1]
        self.returningBudget, self.returningHipsterBudget, self.oneTimeBudget = budgets[0], budgets[1], budgets[2]
        self.revenue, self.tips = [], []
        self.nDailyTimeSlots, self.dailyTimeSlots = len(daily_time_slots), daily_time_slots

        # Initialize list of Returning Customers
        self.nReturningCustomers, self.returningCustomers = n_returning_customers, []

        for i in range(1, self.nReturningCustomers + 1):
            cid = "CID_r" + str(i)
            if not numpy.random.choice([True, False], p=self.pHipster):
                self.returningCustomers.append(ReturningCustomer(cid, False, self.returningBudget))
            else:
                self.returningCustomers.append(ReturningCustomer(cid, True, self.returningHipsterBudget))

        self.SimulationStartDate = None
        self.DateRange = None
        self.nDays = None

    def set_date(self, start_date: str, number_of_days: int):
        self.SimulationStartDate = start_date
        self.nDays = number_of_days

        # Initialize date range for the simulation
        self.DateRange = pd.date_range(start=self.SimulationStartDate, periods=self.nDays)

        # Remove the leap year because we do not recognize leap years! :)
        leap = []
        for each in self.DateRange:
            if each.month == 2 and each.day == 29:
                leap.append(each)
        self.DateRange = self.DateRange.drop(leap)

        # if leap day(s) is/are removed, add equivalent number of days to the end of the DateRange
        while len(self.DateRange) < self.nDays:
            date1 = pd.DatetimeIndex(data=[self.DateRange[len(self.DateRange) - 1] + pd.DateOffset(1)])
            if date1.month == 2 and date1.day == 29:
                date1 = pd.DatetimeIndex(data=[self.DateRange[len(self.DateRange) - 1] + pd.DateOffset(2)])
            self.DateRange = self.DateRange.append(date1)

    def get_date(self, time_slot_number: int, day_number: int):
        return self.DateRange[day_number].replace(hour=int(self.dailyTimeSlots[time_slot_number].split(":")[0]),
                                                  minute=int(self.dailyTimeSlots[time_slot_number].split(":")[1]))
        # return str(self.DateRange[day_number]) + " " + str(self.dailyTimeSlots[time_slot_number])

    def most_expensive_purchase(self) -> int:
        d, f = 0, 0
        for i in range(0, len(self.pricesDrink)):
            if d < self.pricesDrink[i].price:
                d = self.pricesDrink[i].price
        for i in range(0, len(self.pricesFood)):
            if f < self.pricesFood[i].price:
                f = self.pricesFood[i].price
        return f + d

    def start(self, start_date: str, num_days: int):
        self.set_date(start_date, num_days)
        ot_c = 0  # counter of OneTimeCustomers to create ids
        returning_customers_copy = self.returningCustomers.copy()  # make a copy of the returning customers list

        for k in range(0, len(self.DateRange)):
            # Create local daily variables
            daily_customer_list, daily_purchases = [], []  # list of customers on a given day, list of daily purchases
            special = False
            tips_day, revenue_day = 0, 0  # daily revenue and daily tips

            for i in range(0, self.nDailyTimeSlots):
                choice = numpy.random.choice([0, 1], p=self.pMainType)
                if choice == 0 and returning_customers_copy:
                    random_returning_customer = numpy.random.choice(returning_customers_copy)
                    while random_returning_customer.budget < self.most_expensive_purchase() and returning_customers_copy:
                        returning_customers_copy.remove(random_returning_customer)
                        if returning_customers_copy:  # if customerListR_tmp is not empty
                            random_returning_customer = numpy.random.choice(returning_customers_copy)
                    # if customer is a returning customer, then append to list of customers a random regular customer
                    if returning_customers_copy:
                        daily_customer_list.append(random_returning_customer)
                    else:
                        special = True
                if choice == 1 or special or not returning_customers_copy:
                    special = False
                    # if customer is a OneTimeCustomer, then create a new OneTimeCustomer object
                    cid = "CID_ot" + str(ot_c)
                    ot_c += 1
                    daily_customer_list.append(
                        OnetimeCustomer(cid, numpy.random.choice([True, False], p=self.pTripAdvisor)))

                if sum(self.pDrink[i]) == 0:
                    random_drink = Item("NA", 0)
                else:
                    random_drink = numpy.random.choice(self.pricesDrink, p=self.pDrink[i])
                if sum(self.pFood[i]) == 0:
                    random_food = Item("NA", 0)
                else:
                    random_food = numpy.random.choice(self.pricesFood, p=self.pFood[i])
                daily_purchases.append(Purchase(random_drink, random_food, self.get_date(i, k)))
                daily_customer_list[i].buy(daily_purchases[i], daily_customer_list[i].tip)
                tips_day += daily_customer_list[i].tip
                revenue_day = revenue_day + daily_purchases[i].total + daily_customer_list[i].tip

            # append daily revenue and tips to the list
            self.revenue.append(revenue_day)
            self.tips.append(tips_day)

        # Combine revenue and tips with DateRange and change format to dataframe for easy reading
        self.revenue = DataFrame({"Time": self.DateRange, "revenue": self.revenue})
        self.tips = DataFrame({"Time": self.DateRange, "tips": self.tips})

    def plot(self, var: str, diff: bool):
        fig_title = "Simulated " + var + " from " + self.DateRange[0].strftime("%m/%d/%Y") + " to " + str(
            self.DateRange[self.nDays - 1].strftime("%m/%d/%Y"))
        if var == "revenue" and not diff:
            ax = self.revenue.plot(x="Time", y="revenue", label="Revenue", figsize=(16, 8), title=fig_title)
        elif var == "tips":
            ax = self.tips.plot(x="Time", y="tips", label="Tips", figsize=(16, 8), title=fig_title)
        elif var == "revenue" and diff:
            original = self.get_original_revenue(self.nDays)
            revenue_difference = DataFrame({"Time": self.revenue["Time"],
                                            "revenueDiff": original["revenue"] - self.revenue["revenue"]})
            ax = revenue_difference.plot(x="Time", y="revenueDiff",
                                         label="Revenue (deviation from original)", figsize=(16, 8), title=fig_title)
        else:
            print("Only <revenue> or <tips> can be plotted for now")
            return 0
        if self.nDays > 3 * 365:
            print("reducing ticks on x-axis to every 3 months")
            ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))  # to display ticks every 3 months

        ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))  # to set how dates are displayed
        plt.legend()
        plt.show()

    def display_returning_customers_purchase_history(self):
        for i in range(0, len(self.returningCustomers)):
            print("Purchase history for customer %s" % self.returningCustomers[i].customerID)
            self.returningCustomers[i].purchase_history()
            print()

    def get_original_revenue(self, n_days: int) -> DataFrame:
        df = pd.read_csv(path.abspath(path.join(__file__, "../../.."))+"/data/Coffeebar_2016-2020.csv", delimiter=";")
        df["TIME"] = pd.to_datetime(df.TIME)

        def count(key: str):
            if key == "FOOD":
                var = self.pricesFood
            elif key == "DRINKS":
                var = self.pricesDrink
            else:
                return "Error"
            df['Time'] = df['TIME'].dt.strftime('%Y-%m-%d')
            output = df[key].groupby([df["Time"], df[key]]).count().unstack(level=1)
            output.fillna(0)
            for i in range(0, len(var)):
                output[var[i].name] *= var[i].price
            output["revenue"] = output.sum(axis=1)
            return output.iloc[:, len(var)]

        orig = count("FOOD") + count("DRINKS")
        orig = orig.reset_index()
        orig["Time"] = pd.to_datetime(orig["Time"])
        orig = DataFrame(orig.head(n=n_days))
        return orig
